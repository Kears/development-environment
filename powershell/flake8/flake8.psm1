# https://gitlab.com/pycqa/flake8


$pythonPath = [System.IO.Path]::combine('C:', 'virtual-environments', 'flake8',
    'Scripts', 'python.exe')

function Lint {
    param ( [parameter(mandatory)] [string] $path )
    start-process `
        -filePath $pythonPath `
        -argumentList "-m flake8 `"$path`"" `
        -noNewWindow `
        -wait
}

export-moduleMember -function 'lint'
