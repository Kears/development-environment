# https://github.com/AlDanial/cloc


$clocPath = [System.IO.Path]::combine('C:', 'virtual-environments', 
    'cloc-1.86.exe')

function Count {
    param ( [parameter(mandatory)] [string] $targetDirectory )
    start-process `
        -filePath $clocPath `
        -argumentList $targetDirectory `
        -noNewWindow `
        -wait
}

export-moduleMember -function 'count'
