# https://flask.palletsprojects.com/en/1.1.x/cli/


$pythonPath = [System.IO.Path]::combine('C:', 'virtual-environments', 'flask',
    'Scripts', 'python.exe')
$localHost = 'http://127.0.0.1:5000/'

function Start-Flask-Server {
    set-clipboard -value $localHost
    $env:FLASK_APP = 'py/flask_server'
    $env:FLASK_ENV = 'development'
    start-process `
        -filePath $pythonPath `
        -argumentList '-B -m flask run' `
        -noNewWindow `
        -wait
}

start-flask-server
