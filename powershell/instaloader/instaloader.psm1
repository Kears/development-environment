# https://instaloader.github.io/


# ------ Constants ------ #

$pythonPath = [System.IO.Path]::combine('C:', 'virtual-environments',
    'downloaders', 'Scripts', 'python.exe')

$generalArguments = @(
    '--login=pobox04947'
    '--dirname-pattern={target}'
    '--no-captions'
    '--no-metadata-json'
    '--no-video-thumbnails'
)
$imageArguments = '--no-videos'
$videoArguments = '--no-pictures', '--no-profile-pic'
$space = ' '


# ------ Functions ------ #

function Update-Images {
    param ( [parameter(mandatory)] [string] $profile )
    $arguments = $generalArguments + $imageArguments + '--fast-update'
    get-content -arguments $arguments -profile $profile
}

function Check-Images {
    param ( [parameter(mandatory)] [string] $profile )
    $arguments = $generalArguments + $imageArguments
    get-content -arguments $arguments -profile $profile
}

function Update-Video {
    param ( [parameter(mandatory)] [string] $profile )
    $arguments = $generalArguments + $videoArguments + '--fast-update'
    get-content -arguments $arguments -profile $profile
}

function Get-Content {
    param ( $arguments, $profile )
    $arguments = $arguments -join $space
    start-process `
        -filePath $pythonPath `
        -argumentList "-m instaloader $arguments $profile" `
        -noNewWindow `
        -wait
}

$toExport = 'update-images', 'check-images', 'update-video'
export-moduleMember -function $toExport
