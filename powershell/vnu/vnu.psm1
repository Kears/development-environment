# https://github.com/validator/validator


# ------ Constants ------ #

$vnuPath = [System.IO.Path]::combine('C:', 'virtual-environments', 
    'vnu-runtime-image', 'bin', 'vnu.bat')

$generalOptions = @( '--asciiquotes')


# ------ Functions ------ #

function Validate-All {
    param ( [parameter(mandatory)] [string] $path )
    $options = '--also-check-css', '--also-check-svg'
    validate -options $options -path $path
}

function Validate-HTML {
    param ( [parameter(mandatory)] [string] $path )
    $options = @('--skip-non-html')
    validate -options $options -path $path
}

function Validate-CSS {
    param ( [parameter(mandatory)] [string] $path )
    $options = @('--skip-non-css')
    validate -options $options -path $path
}

function Validate-SVG {
    param ( [parameter(mandatory)] [string] $path )
    $options = @('--skip-non-svg')
    validate -options $options -path $path
}

function Validate {
    param ( $options, $path )
    $options += $generalOptions
    $space = ' '
    $options = $options -join $space
    start-process `
        -filePath $vnuPath `
        -argumentList "$options `"$path`"" `
        -noNewWindow `
        -wait
}


$toExport = 'validate-all', 'validate-HTML', 'validate-CSS', 'validate-SVG'
export-moduleMember -function $toExport
