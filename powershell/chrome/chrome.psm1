<# https://peter.sh/experiments/chromium-command-line-switches/

The Chrome command line switch --auto-open-devtools-for-tabs gets ignored if a
preexisting instance of Chrome is open. Specifically, if the first instance
does not have DevTools open, all subsequent instances and tabs will not open
DevTools regardless of the command line switch. If the first instance has
DevTools open, all subsequent ones will automatically open DevTools. 

#>

$chromePath = [System.IO.Path]::combine('C:', 'Program Files (x86)', 
    'Google', 'Chrome', 'Application', 'chrome.exe')

function Start-From-Clipboard {
    [string] $url = get-clipboard
    $arguments = "$url --incognito --auto-open-devtools-for-tabs"
    start-process `
        -filePath $chromePath `
        -argumentList $arguments
}

export-moduleMember -function 'start-from-clipboard'
