# https://docs.python.org/3/library/http.server.html


# ------ Constants ------ #

$pythonPath = [System.IO.Path]::combine('C:', 'Program Files', 'Python37',
    'python.exe')


# ------ Functions ------ #

function Start {
    param ( 
        [parameter(mandatory)]
        [string] $HTMLFilepath,
        [int] $parentLevel = 0
    )
    $hostDir = get-host-directory $HTMLFilepath $parentLevel
    $arguments = "-m http.server 8000 --directory $hostDir"
    $url = generate-host-URL $HTMLFilepath $parentLevel
    set-clipboard -value $url
    start-process `
        -filePath $pythonPath `
        -argumentList $arguments `
        -noNewWindow `
        -wait
}

function Get-Host-Directory {
    param ( [string] $HTMLFilepath, [int] $parentLevel )
    $hostDir = split-path $HTMLFilepath -parent
    for ($i = 0; $i -lt $parentLevel; $i++) {
        $hostDir = split-path $hostDir -parent
    }
    return $hostDir
}

function Generate-Host-URL {
    param ( [string] $HTMLFilepath, [int] $parentLevel )
    $url = 'http://localhost:8000'
    $toAppend = new-object System.Collections.ArrayList
    for ($i = 0; $i -le $parentLevel; $i++) {
        $leaf = split-path $HTMLFilepath -leaf
        $toIgnore = $toAppend.add($leaf)
        $HTMLFilepath = split-path $HTMLFilepath -parent
    }
    $toAppend.reverse()
    foreach ($leaf in $toAppend) {
        $url += '/' + $leaf
    }
    return $url
}

export-moduleMember -function 'start'
